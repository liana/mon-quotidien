angular.module('starter.controllers')

.controller('HomeCtrl', function($rootScope, $scope, $ionicPopup, $location, $http, $ionicModal) {

$scope.open_sel = function($index) {
  $rootScope.sel = $index;
 	$rootScope.init_data($rootScope.protos[$rootScope.sel]); //init the data
	$location.path("sel_proto");

}

$scope.delete_fiche = function(index)
{
  $ionicPopup.confirm({
        title: 'System warning',
        template: 'are you sure you want to delete the data '+$rootScope.protos[index].categorie.title+'?'
      }).then(function(res) {
	      if (res) {
	      $http.get("http://188.166.169.98/v2/del_fiche.php/?id_fiche="+$rootScope.protos[index].id+"&user="+$rootScope.user)
	      .then(function(response) {
		      	console.log(response.data);
			$rootScope.protos.splice(index, index+1);
			});
	      }
	      })
}

$scope.go_phase = function() {
$location.path("phase");
}
$scope.open_chart = function($index) {
  $rootScope.sel = $index;
  $rootScope.sender = "";
//  $rootScope.init_chart($rootScope.saved[$index]);
	$rootScope.get_data_stat($rootScope.protos[$rootScope.sel].id, $rootScope.user);
  $location.path("sel_chart");
}

})

.controller('AccountCtrl', function($http, $scope, $rootScope, $ionicPopup) {

$scope.add_user = function() {
if ($rootScope.userauth.length < 10)
{
$scope.data = {};
  var myPopup = $ionicPopup.show({
    template: '<input type="text" ng-model="data.new" placeholder="email@email.com">',
    title: 'Ajouter l\'email du nouveau utilisateur',
    subTitle: 'il pourra ensuite acceder au projet lors de sa prochaine connexion a l\'app',
    scope: $scope,
    buttons: [{ text: 'Cancel' }, {text: '<b>Save</b>', type: 'button-royal', onTap: function(e) {if (!$scope.data.new) {
          } else {
$http.get("http://188.166.169.98/v2/add_user_to_patient.php/?email="+$scope.data.new+"&id="+$rootScope.project.id+"&right=1")
        .then(function(response) {
		console.log(response.data);
		if (strncmp(response.data, "ERROR", 5) != 0) {
			$rootScope.userauth.push($scope.data.new);
      }
    });
    return $scope.data.new;}}}]});
  myPopup.then(function(res) {
    console.log('Tapped!', res);

  });
}
};

  $rootScope.settings = {
    enableCollect: true,
    enableRoot: false
  };
  $scope.del_proto = function($index) {
    //a changer
    /*
    var confirmPopup = $ionicPopup.confirm({
     title: 'delete ' + $rootScope.protos[$index].categorie.title + '?',
     template: 'Are you sure you want to delete this proto?'
   });
   confirmPopup.then(function(res) {
     if(res) {
       console.log('You are sure');
       $rootScope.protos.splice($index, 1);
     } else {
       console.log('You are not sure');
     }
   });
  */};

})

.controller('ChartsCtrl', function($scope, $rootScope, $location, $ionicPopup, $http) {

$scope.wanted = [];
$scope.choosen = [];
$scope.nb_choose = 0;
	for (var i = 0; i < $rootScope.protos.length; i++)
	{
		$scope.wanted[i] = 0;
		$scope.choosen[i] = [];
		for (var a = 0; a < $rootScope.protos[i].qas.length; a++)
			$scope.choosen[i].push(0);
	}
$scope.select_it = function($index, $$index)
{
	if ($scope.choosen[$$index][$index] == 1)
	{
		$scope.choosen[$$index][$index] = 0;
		$scope.nb_choose--;
	}
	else
	{
		$scope.choosen[$$index][$index] = 1;
		$scope.nb_choose++;
	}
}
$scope.items = [
      {id: '1', name: 'histogramme'},
        {id: '2', name: 'graphe avec phases'}
    ];

$scope.see_correlation = function()
{
	if ($scope.nb_choose > 0)
	{
		$location.path("chartvisu");
	}
}

function get_data_chart() {

    $http.get("http://188.166.169.98/mon-quotidien/getdata.php/?email="+$rootScope.user
+"&name="+$rootScope.project+"&name_cat="+$rootScope.protos[$rootScope.sel].categorie.title+"&name_id="+String($rootScope.sel)).then(function(resp) {
	console.log(resp.data);
	return (resp.data);
});

}
})

;
