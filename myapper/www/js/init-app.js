angular.module('starter.controllers', ['ti-segmented-control'])

.run(function($http, $ionicPlatform, $state, $rootScope, $location) {

 // Disable BACK button on home
  $ionicPlatform.registerBackButtonAction(function(event) {
    if (true && $state.current.name == "log" ) { // your check here
      $ionicPopup.confirm({
        title: 'System warning',
        template: 'are you sure you want to exit?'
      }).then(function(res) {
        if (res) {
          ionic.Platform.exitApp();
        }
      })
    }
  }, 100);

$rootScope.go_back = function()
{
	$rootScope.current_data = [];
	$location.path("tab.home");
};

$rootScope.get_data_stat = function(id_fiche, email)
{
  /*
	//WE CHECK IF INTERNET IS CONNECTED
	if(window.Connection) {
		if (navigator.connection.type == Connection.NONE)
		{
			//IF NOT WE PUT A POPUP TELLING US WE CAN'T ACCESS THIS PAGE
			var alertPopup = $ionicPopup.alert({
		     	title: 'You can\' access this page',
		     	template: 'sorry you must have an internet connection to access this page ...'
		   	});
		}
		else
		{*/
			$http.get("http://188.166.169.98/v2/get_data.php/?id_fiche="+id_fiche+"&email="+email).then(function(response)
					{
					console.log("data get");
					console.log(response.data);
					$rootScope.current_data = response.data;
					$rootScope.chart_visu = create_chart("phases",
						{ datas:[{datas: [1, 2, 3, 4], phase: 0},
						{datas: [1, 2, 10, 4], phase: 0},
						{datas: [0, 10, 1, 4], phase: 1},
						{datas: [1, 2, 3, 4], phase: 1},
						{datas: [1, 2, 3, 4], phase: 0}],
name: ""+ $rootScope.project.name,
description:"",
height: 300,
infos: { phases_name: ["observation", "recompense 1"], datas_name: ["courbe 0", "courbe 1", "courbe 2", "courbe 3"] }});
					});
			$http.get("http://188.166.169.98/v2/get_push.php/?id_fiche="+id_fiche+"&email="+email)
			.then(function(response)
			{
				console.log(response.data);
				var datas = [];
				$rootScope.current_push = response.data;
			});
		/*}
	}*/
}

$rootScope.go_choose = function()
{
	$rootScope.userauth = [];
	$location.path("choose");
};

$rootScope.user_get_project = function($user, $scope) {
	$http.get("http://188.166.169.98/v2/search_patient_for_user.php/?user_mail="+$user)
		.then(function(response) {
				console.log(response.data);
				if (strncmp(response.data, "0", 1) != 0)
				{
				var str = String(response.data);
				var t_tmp = str.split("\n");
				$rootScope.projects = [];
				for (var i = 0; i < t_tmp.length; i++)
				{
				var t_tmp_2 = t_tmp[i].split(",");
				$rootScope.projects.push({id: t_tmp_2[0], name: t_tmp_2[1], color: t_tmp_2[2]});
				}
				$rootScope.projects.pop();
				}
				});
}

$rootScope.init_data = function($data)
{
	$rootScope.current_data = [];
	for (var i = 0; i < $data.qas.length; i++)
	{
		$rootScope.current_data[i] = { type: $data.qas[i].a.id, answer: (($data.qas[i].a.id == 4) ? "" : 0 ) };
	}
}

$rootScope.get_all_user = function() {
	$http.get("http://188.166.169.98/v2/get_user_for_patient.php/?id="+$rootScope.project.id)
		.then(function(response) {
				console.log("get all user");
				console.log(response.data);
				if (strncmp(JSON.stringify(response.data), "Fail", 4) != 0)
				{
					$rootScope.userauth = response.data;
				}
		});
}

$rootScope.user = "";
$rootScope.userauth = [];
$rootScope.project = "";
$rootScope.sel = 0;
$rootScope.get_protos = function()
{
	$http.get("http://188.166.169.98/v2/get_protos.php/?email="+$rootScope.user+"&id="+$rootScope.project.id)
	.then(function(response)
	{
				console.log(response.data);
				if (strncmp(response.data, "ERROR", 5) != 0)
				{
					$rootScope.protos = [];
					var a = 0;
					var tab = String(response.data).split("\n");
					for (var i = 0; i < tab.length - 1; i += 3)
					{
						var ti = tab[i].split(",");
						var tmp_cat = {title: ti[0],
						logo: ti[1]};
						var color = ti[2];
						var tq = tab[i + 1].split(";");
						var ta = tab[i + 2].split(";");
						var tmp = [];
						for (var j = 0; j < tq.length; j++)
						{
							tmp.push({q: tq[j], a: {id: String(ta[j]) }});
						}
					$rootScope.protos.push({ id: ti[4], categorie: tmp_cat, qas: tmp, creation_time: new Date(ti[3]).getTime(), color: color});
					a++;
					}
					window.localStorage.setItem("protos", JSON.stringify($rootScope.protos));
				}
	});
}

//I make you an example, it's an array of a object contaning the categorie choose ande the qas (questions and reponses)
$rootScope.projects = [];
$rootScope.choose = 0;
$rootScope.categories = [
{
title: "manger", logo: "http://www.westbaptist.org.nz/wp/wp-content/uploads/2010/03/eat-icon.jpg"
},
{
title: "hygiene",
       logo: "http://partnermed.byhirvi.no/images/modules/icon_hygiene.png"
},
{
title: "dormir",
       logo: "https://wfcastle33.files.wordpress.com/2014/03/icon-sleep.png"
},
{
title: "sport",
       logo: "http://www.dreamix-studio.com/runtrainer/runtrainer_icon.png"
},
{
title: "devoir maison",
       logo: "http://www.iconshock.com/img_jpg/XMac/education_icons/jpg/256/homework_icon.jpg"
},
{
title: "comportement",
       logo: "https://www.seemescotland.org/media/1041/icon_mission_behavior_200.png?anchor=center&mode=crop&quality=90&width=160&heightratio=1&slimmage=true&rnd=130574217830000000"
}
];
$rootScope.protos = [
//element 1
{
categorie: {title: "example", logo: "http://vignette2.wikia.nocookie.net/fantendo/images/5/5b/Color_wheel_dock_icon_by_andybaumgar-d3ezjgc.png/revision/latest?cb=20141006223915"},
		   qas: [
		   {q: "date/heure", a:  {id: '3', name: 'date'} },
		   {q: "avec quel adulte est il parti au toilette?", a:  {id: '7', name: 'qui'} },
		   {q: "a t'il fait pipi?", a:  {id: '1', name: 'oui/non/accident'} },
		   {q: "a t'il utilise la Selle?", a:  {id: '1', name: 'oui/non/accident'} },
		   {q: "est-ce une demande spontanee?", a:  {id: '6', name: 'oui/non/accident'} },
		   {q: "Remarque", a:  {id: '4', name: 'oui/non/accident'} }
	   ],
		   creation_time: 0 //date of creation of this proto (not important now)
}
];
$rootScope.saved = [
//FOR 1 CATEGORIE (here example)
{
all_datas: [{
datas: [{type: '3', answer: 0}, {type: '7', answer: 0}, {type: '1', answer: 0}],
	       creation_time: 0
	   },
		   {
datas: [{type: '3', answer: 0}, {type: '7', answer: 0}, {type: '1', answer: 1}],
       creation_time: 0
		   },
		   {
datas: [{type: '3', answer: 0}, {type: '7', answer: 0}, {type: '1', answer: 2}],
       creation_time: 0
		   },
		   {
datas: [{type: '3', answer: 0}, {type: '7', answer: 0}, {type: '1', answer: 0}],
       creation_time: 0
		   }
	   ],
		   categorie_id: "example"
}
];

$rootScope.get_local = function(key)
{
	var r = window.localStorage.getItem(key);
	console.log("get_local: "+key);
	console.log(r);
	return (r);
}

$rootScope.user = $rootScope.get_local("user");
$rootScope.projects = $rootScope.get_local("projects");
$rootScope.protos = JSON.parse($rootScope.get_local("protos"));
console.log("print user, then projects, then protos");
console.log($rootScope.user);
console.log($rootScope.projects);
console.log($rootScope.protos);
if ($rootScope.user == null)
	$location.path("log");
if (!$rootScope.projects)
{
	console.log("ok");
	$rootScope.user_get_project($rootScope.user);
	$location.path("choose");
}

});
