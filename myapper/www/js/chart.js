function	create_chart(type_chart, cont)
{
	if (type_chart == "phases")
		return (create_phases(cont));
	if (type_chart == "histo")
		return (create_histo(cont));
	if (type_chart == "normal")
		return (create_normal(cont));
}

function getRandomColor() {
   var letters = '0123456789ABCDEF';
   var color = '';
   for (var i = 0; i < 6; i++ ) {
       color += letters[Math.floor(Math.random() * 16)];
   }
   return (color);
}

function	create_histo(cont)
{
	var option = {
            chart: {
                type: 'historicalBarChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 65,
                    left: 50
                },
		x: function(d){return d[0];},
		y: function(d){return d[1]/100000;},
                showValues: true,
                valueFormat: function(d){
                    return d3.format(',.1f')(d);
                },
                duration: 100,
                xAxis: {
                    axisLabel: 'X Axis',
                    tickFormat: function(d) {
                        return d3.time.format('%x')(new Date(d))
                    },
                    rotateLabels: 30,
                    showMaxMin: false
                },
                yAxis: {
                    axisLabel: 'Y Axis',
                    axisLabelDistance: -10,
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                },
                tooltip: {
                    keyFormatter: function(d) {
                        return d3.time.format('%x')(new Date(d));
                    }
                },
                zoom: {
                    enabled: true,
                    scaleExtent: [1, 10],
                    useFixedDomain: false,
                    useNiceScale: false,
                    horizontalOff: false,
                    verticalOff: true,
                    unzoomEventType: 'dblclick.zoom'
                }
            }
        };
	var data = cont.datas;
	return ({options: option, data: data});
}

function	create_phases(cont)
{
	var option = { chart: {
	type: 'lineChart',
	height: cont.height,
	margin : {
	top: 20,
	right: 20,
	bottom: 40,
	left: 55
	},

	x: function(d){ return d.x; },
	y: function(d){ return d.y; },
   useInteractiveGuideline: true,
   dispatch: {
stateChange: function(e){ console.log("stateChange"); },
	     changeState: function(e){ console.log("changeState"); },
	     tooltipShow: function(e){ console.log("tooltipShow"); },
	     tooltipHide: function(e){ console.log("tooltipHide"); }
   },
xAxis: {
axisLabel: 'jour',
	   tickFormat: function(d){
		   return (d3.time.format('%x')(new Date(d)));
	   },
	      rotateLabels: 30,
	      showMaxMin: false,
	      showLegend: false
       },
yAxis: {
axisLabel: 'Quantite',
	   tickFormat: function(d){
		   return d3.format('.02d')(d);
	   },
axisLabelDistance: -10
       },
callback: function(chart){
		  console.log("!!! lineChart callback !!!");
	  }
	},
title: {
enable: true,
	text: 'observation pour '+cont.name
       },
subtitle: {
enable: true,
	text: 'sous titre: '+cont.description,
	css: {
		'text-align': 'center',
		'margin': '10px 13px 0px 7px'
	}
	  },
};
var data = data_with_phase(cont.datas, cont.infos);

return ({options: option, data: data});
}

function	create_normal(cont)
{
	var option = { chart: {
	type: 'lineChart',
	height: cont.height,
	margin : {
	top: 20,
	right: 20,
	bottom: 40,
	left: 55
	},

	x: function(d){ return d.x; },
	y: function(d){ return d.y; },
   useInteractiveGuideline: true,
   dispatch: {
stateChange: function(e){ console.log("stateChange"); },
	     changeState: function(e){ console.log("changeState"); },
	     tooltipShow: function(e){ console.log("tooltipShow"); },
	     tooltipHide: function(e){ console.log("tooltipHide"); }
   },
xAxis: {
axisLabel: 'jour',
	   tickFormat: function(d){
		   return (d3.time.format('%x')(new Date(d)));
	   },
	      rotateLabels: 30,
	      showMaxMin: false,
	      showLegend: false
       },
yAxis: {
axisLabel: 'Quantite',
	   tickFormat: function(d){
		   return d3.format('.02d')(d);
	   },
axisLabelDistance: -10
       },
callback: function(chart){
		  console.log("!!! lineChart callback !!!");
	  }
	},
title: {
enable: true,
	text: 'observation pour '+cont.name
       },
subtitle: {
enable: true,
	text: 'sous titre: '+cont.description,
	css: {
		'text-align': 'center',
		'margin': '10px 13px 0px 7px'
	}
	  },
};
	var data = data_normal(cont.datas, cont.infos);
	return ({options: option, data: data});
}

function data_normal(data, data_info) 
{
	var chart_data = [];
	var chart_info = [];
	var return_data = [];
	var max;
	var courbe;
	var phases;
	var len;

	max = 0;
	courbe = 0;
	phases = 0;
	len = 0;
	chart_data[0] = [];
	for (var a = 0; a < data.length; a++)
	{
		for (var i = 0; i < data[a].datas.length; i++)
		{
			if (i > courbe)
			{
				courbe++;
				chart_data[i] = [];
			}
			if (max < data[a].datas[i])
				max = data[a].datas[i];
			chart_data[i].push({ x: a, y: data[a].datas[i] });
		}
	}
	for (var a = 0; a < chart_data.length; a++)
	{
		return_data.push({
		values: chart_data[a],
		key: data_info.datas_name[a],
		//color: "#ff70e
		strokeWitdh: 2,
		classed: 'dashed'
		});
	}
	return (return_data);
}

function data_with_phase(data, data_info) 
{
	var chart_data = [];
	var chart_phases = [];
	var chart_info = [];
	var return_data = [];
	var max;
	var courbe;
	var phases;
	var len;

	max = 0;
	courbe = 0;
	phases = 0;
	len = 0;
	chart_data[0] = [];
	for (var a = 0; a < data.length; a++)
	{
		if (phases == data[a].phase)
		{
			phases += 1; 
			chart_phases[data[a].phase] = [];
		}
		for (var i = 0; i < data[a].datas.length; i++)
		{
			if (i > courbe)
			{
				courbe++;
				chart_data[i] = [];
			}
			if (max < data[a].datas[i])
				max = data[a].datas[i];
			chart_data[i].push({ x: a, y: data[a].datas[i] });
		}
	}
	console.log(chart_data);
	for (var a = 0; a < data.length; a++)
	{
		chart_phases[data[a].phase].push({x: a, y: max});
		for (var i = 0; i < phases; i++)
		{
			if (i != data[a].phase)
				chart_phases[i].push({x: a, y: null});
		}
	}
	console.log(chart_phases);
	for (var a = 0; a < chart_data.length; a++)
	{
		return_data.push({
		values: chart_data[a],
		key: data_info.datas_name[a],
		//color: "#ff70e
		strokeWitdh: 2,
		classed: 'dashed',
	      	showLegend: false
		});
	}
	for (var a = 0; a < chart_phases.length; a++)
	{
		return_data.push({
		values: chart_phases[a],
		key: data_info.phases_name[a],
		bar: true,
		area: true
		});
	}
return (return_data);
}
